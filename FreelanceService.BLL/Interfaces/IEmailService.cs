﻿using System.Threading.Tasks;

namespace FreelanceService.BLL.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
