﻿namespace FreelanceService.Web.Models
{
    public class EmailViewModel
    {
        public string NameUser { get; set; }
        public string EmailUser { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }


    }
}
